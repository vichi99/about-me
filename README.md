# About me

[![Pipeline Status](https://gitlab.com/vichi99/vichi99.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/vichi99/vichi99.gitlab.io/-/commits/master)
[![Python version](https://img.shields.io/badge/python-3.10-blue.svg)](https://www.python.org/downloads/release/python-3100)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/vichi99/issue-tracker/-/blob/master/LICENSE)
[![Python Black Code Style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)

An Easy Way to create a online resume with python and docker to the gitlab pages or other domains.

**Demo:** [https://vichi99.gitlab.io/](https://vichi99.gitlab.io/)

This project is inspired by: [https://github.com/suheb/resume](https://github.com/suheb/resume)

# Development

## Clone

```sh
git clone https://gitlab.com/vichi99/vichi99.gitlab.io
```

## Requirements

- [docker, docker-compose](https://www.docker.com/)
- [poetry](https://python-poetry.org/) - optionally, for local IDE and adding pip packages

  ```sh
  $ pip install poetry
  $ poetry install
  $ poetry add package_name # for adding package
  # The below is no needed. Export requirements are performed in dockerfile.
  $ poetry export --without-hashes -f requirements.txt --output app/requirements
  ```

## Run

```sh
$ docker-compose up # start container
$ docker-compose down -v # stop and remove containers and remove volumes
$ docker-compose build # for rebuild docker image (etc. if requirements are added)
```

web should be on [http://localhost:8000](http://localhost:8000)

# Configuration

- described pages [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
- describe domain names [gitlab domain names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names)

```
SITEURL = "https://vichi99.gitlab.io" # read about gitlab domain names
# sidebar links
NAME = ""
TAGLINE = ""
EMAIL = ""
PHONE = None
LINKEDIN = ""
GITLAB = ""
GITHUB = ""
TWITTER = None

CAREER_SUMMARY = ""

PROJECT_INTRO = ""
PROJECTS = [{"title": "", "tagline": ""}]


EXPERIENCES = [
    {
        "job_title": "Python Web Developer",
        "time": f"July 2021 - present",
        "company": "XY, Prague - remote full time",
        "about_company": "",
        "details": ""
    },
]

SKILLS = [
    {"title": "Python", "level": ""},
]

EDUCATIONS = [
    {
        "name": "",
        "degree": "",
        "field": "",
        "time": "",
    },
]

LANGUAGES = [
    {"name": "Czech", "description": "Native"},
]
INTERESTS = ["Sports"]
```

![resume](resume-screen.png)

The theme is responsive.

![resume-responsive](resume-responsive.png)

# CI/CD

## Before commit

- These steps must be done before pushing commit, otherwise the pipeline in gitlab will crash.

```sh
$ isort app
$ black app
$ flake8 app
```

# Deploy

Deployment to the gitlab pages is provided by CI/CD - `.gitlab-ci.yml`, just push it into the master.

Check out for more themes: [Pelican Themes](http://www.pelicanthemes.com/)
