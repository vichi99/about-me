AUTHOR = "jan vicha"
SITENAME = "about me"
SITEURL = "https://vichi99.gitlab.io"

PATH = "content"
OUTPUT_PATH = "public"

TIMEZONE = "Europe/Rome"

DEFAULT_LANG = "cs"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
THEME = "theme"

DEFAULT_PAGINATION = False

CSS_FILE = "main-6.css"


# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# sidebar links
NAME = "Jan Vícha"
TAGLINE = "Python web developer"
EMAIL = "john.vicha@seznam.cz"
PHONE = None
LINKEDIN = "jan-vícha-2b699612b"
GITLAB = "vichi99"
GITHUB = "vichi99"
TWITTER = None

CAREER_SUMMARY = "I am a backend developer primarily using Python, with limited involvement in front-end development. My interests lie in the CI/CD processes, and I value organised work with well-defined workflows."


PROJECT_INTRO = ""
PROJECTS = [
    {
        "title": "Issue Tracker (side project)",
        "url": "https://gitlab.com/vichi99/issue-tracker",
        "tagline": "Issue tracker to help manage tickets. \
            Tickets can be managed in Django admin or viewed \
                for list and detail in the API.",
    },
    {
        "title": "About me",
        "url": "https://gitlab.com/vichi99/vichi99.gitlab.io",
        "tagline": "An Easy Way to create an online resume with Python and \
            docker to the Gitlab pages or other domains.",
    },
    {
        "title": "CryptoBoard",
        "url": "https://gitlab.com/vichi99/CryptoBoard",
        "tagline": "The simple Python CLI tool for one currency shows the \
            current crypto balance you purchase. That's all.",
    },
    {
        "title": "Image shuffler",
        "url": "https://github.com/valosekj/image_shuffler",
        "tagline": "Python script for shuffling/mixing of input image(s).",
    },
    {
        "title": "Running clubs",
        "url": "https://gitlab.com/vichi99/running-clubs",
        "tagline": "A repository for a simple directory of running clubs in Asia.",
    },
    {
        "title": "MicroPython",
        "url": "https://github.com/vichi99/MicroPython",
        "tagline": "There is a place for my MicroPython scripts to control the Nodemcu device.",
    },
    {
        "title": "HomePortal",
        "url": "https://github.com/vichi99/HomePortal",
        "tagline": "Control of home devices.",
    },
    {
        "title": "Sunrise",
        "url": "https://github.com/vichi99/sunrise_sunset",
        "tagline": "Responsive React App for view sunrise and sunset by selected date and Country.",
    },
]


EXPERIENCES = [
    {
        "job_title": "Python Web Developer",
        "time": f"September 2022 - present",
        "company": "Skip Pay",
        "about_company": """
        Develops the concept of so-called smart shopping using a credit card that behaves like a debit card.
        It allows you to conveniently and securely handle a purchase in one service, from payment to a possible claim or refund.
        """,
        "details": """
        • Django, Docker<br>
        I am part of one of two backend teams in a well-organized growing company in the CSOB group.
        I'm currently working mainly on a web backend application in Django.
        I am responsible for adding or modifying new features in web applications. Additionally, I'm dedicated to deepening my knowledge of both Python and Django.
        """,
    },
    {
        "job_title": "Python Web Developer",
        "time": f"July 2021 - September 2022 (1 year 3 months)",
        "company": "Proboston",
        "about_company": """A creative advertising agency based in the heart of Prague.""",
        "details": """
        • Django, Django CMS, Docker, Ansible<br>
        In this position, I helped develop several projects based on Django, Docker, ansible, and CI/CD.
        """,
    },
    {
        "job_title": "Co-Founder",
        "time": "October 2021 - April 2022 (7 months)",
        "company": "NotDevs, Prague - part time",
        "about_company": """A young, growing software company.""",
        "details": """""",
    },
    {
        "job_title": "Python Web Developer",
        "time": "November 2020 - June 2021 (8 months)",
        "company": "NotDevs and Proboston, Prague - remote part time",
        "about_company": """A young, growing software company.""",
        "details": """
        •  Django, Django CMS, Docker, Ansible<br>
        In this position, I helped develop several projects based on Django, Docker, ansible, and CI/CD.
        """,
    },
    {
        "job_title": "Software Developer",
        "time": "May 2019 - June 2021 (2 years and 2 months)",
        "company": "Ekola group, Prague - full time",
        "about_company": """
        The main activities are various acoustic measurements, design of acoustic measures and modelling of acoustic load.
        The company has gradually expanded its activities to include dust measurement,
        lighting conditions and environmental impact assessment of plans and concepts.""",
        "details": """
        • Management / upgrade Python-Flask and C# application.<br>
        • Designing services and applications on Debian.<br>
        • Develop React app.<br>
        I have deepened my knowledge, especially in Python and C#.
        Using Python was mainly to extend the application in Debian and Flask.
        I participated in the creation of an IoT device.
        In C#, I expanded my knowledge by creating applications using the MVVM pattern.
        """,
    },
    {
        "job_title": "Python Developer",
        "time": "June 2019 - January 2020 (8 months)",
        "company": "4Jtech, Prague - remote part time",
        "about_company": """Company 4Jtech s.r.o. was founded in 2015 as a spin-off of the Institute of Fluid
        Mechanics and Thermodynamics, ČVUT in Prague, Faculty of Mechanical Engineering.
        The primary motivation for establishing the company consisted mainly of the possibility of applying the results
        of research and development in the field of modern measuring technology in industrial practice.""",
        "details": """
        • Making a model for reading Ensight binary files in Python.<br>
        • Creating GUI via PyQt.<br>
        • View and make 3D data via PyQtGraph.<br>
        In this remote position, I created a part of the PyQt application
        to read data and coordinates from a binary file and plot them to a 3D graph using PyQtGraph. 

        """,
    },
    {
        "job_title": "LabVIEW Developer",
        "time": "May 2018 - April 2019 (1 year)",
        "company": "ATEsystem s.r.o., Ostrava - full time",
        "about_company": """
        ATEsystem s.r.o. was established at the beginning of 2013 by a
        group of specialists with many years of experience designing and
        implementing testing, measuring and monitoring systems for
        industrial production, research and development.
        The focus of the solutions offered is systems based on visual
        inspection and machine vision using industrial cameras.
        """,
        "details": """
        • Machine vision systems (imaging-based automatic inspection and analysis for the industry).<br>
        • Creation of image processing algorithms with LabVIEW.<br>
        • Solving technical tasks for customers.<br>
        I have deepened my experience with making parts of programs which ensure
        control vision products via LabVIEW. I met with control of the car lights via HSX, CAN, and LIN.

        """,
    },
    {
        "job_title": "IT Technician",
        "time": "May 2017 - April 2018 (1 year)",
        "company": "Workswell, Prague - full time",
        "about_company": """The company is world famous for its products in thermography. 
        This is essentially a stationary WIC thermal camera, thermal
        system ThermoInspector, a thermal imaging system for drones
        Workswell WIRIS and image processing solution.""",
        "details": """
        • Construction, calibration, quality control.<br>
        • Organization of tasks and schedule.<br>
        • Sometimes working on image processing through LabVIEW and VisionPro.<br>
        From the beginning, I worked as a technician who cared for assembling, setting up, calibrating and sending thermo cameras. I learned how to plan individual assignments and keep deadlines properly. I have gained experience in assembling hardware components in antistatic environments and soldering them. Sometimes, I went to the factory to see how machine vision works. About half a year later, I worked on a project on machine vision with my colleague and improved in Cognex SW, like Designer for machine vision and DataMan for barcode readers. I have enhanced the practices for designing machine vision applications. For example, what type of lighting, camera interface, camera type, lens type, triggers, etc ..
        """,
    },
    {
        "job_title": "Software Developer",
        "time": "February 2017 - April 2017 (3 months)",
        "company": "Weppler Group, Ostrava - full time",
        "about_company": "A company specialising in producing microfilters for car engines throughout Western Europe, Australia and Japan.",
        "details": """
        • Direct responsibility for a particular section of microfilter control. <br>
        • Designing/Controlling algorithms for image processing/detection in VisionPro (SW from Cognex – modify script at C#), Raspberry Pi, OpenCV, Python.<br>
        • Find new ways to improve production control.<br>
        My main task was to create an image processing algorithm for microfilter control using VisionPro software. I applied these procedures to an automation machine that performed microfilter control using cameras. I first met with Raspberry Pi, Python and OpenCV image processing library in this work. It was the first time I made a Python script running on Raspberry Pi, which controls the sensor and sends an email. Among my other tasks was communication between Raspberry Pi, PC (C#) and PLC, which was solved by TCP/IP and GPIO DAC. I have only been here for three months, but I have met a lot of things.
        """,
    },
]

BOOKS = [
    {
        "title": "Faktomluva",
        "autor": "Hans Rosling, Ola Rosling & Anna Rosling Rönlund",
        "url": "https://www.melvil.cz/kniha-faktomluva/",
    },
    {
        "title": "Atomové návyky",
        "autor": "James Clear",
        "url": "https://www.melvil.cz/kniha-atomove-navyky/",
    },
    {
        "title": "14 Habits of Highly Productive Developers",
        "autor": "Zeno Rocha",
        "url": "https://14habits.com/",
    },
    {
        "title": "Hluboká práce",
        "autor": "Call Newport",
        "url": "https://www.databazeknih.cz/knihy/hluboka-prace-310127",
    },
    {
        "title": "Čtyři dohody",
        "autor": "Don Miguel Ruiz",
        "url": "https://www.databazeknih.cz/knihy/ctyri-dohody-kniha-moudrosti-starych-tolteku-12114",
    },
    {
        "title": "Umění války",
        "autor": "Sun-c'",
        "url": "https://www.databazeknih.cz/knihy/umeni-valky-30105",
    },
    {
        "title": "Konec prokrastinace",
        "autor": "Petr Ludwig",
        "url": "https://www.konec-prokrastinace.cz/kniha/",
    },
]

SKILLS = [
    {"title": "Python", "level": "80"},
    {"title": "Django", "level": "60"},
    {"title": "Docker", "level": "40"},
    {"title": "Linux/Shell", "level": "30"},
    {"title": "Ansible", "level": "30"},
    {"title": "Machine vision", "level": "30"},
    {"title": "C#", "level": "30"},
    {"title": "React", "level": "20"},
    {"title": "HTML &amp; CSS", "level": "20"},
    {"title": "JavaScript &amp; Typescript", "level": "20"},
]

EDUCATIONS = [
    {
        "name": "Faculty of Electrical Engineering",
        "degree": "only bachelor thesis",
        "field": "Biomedical Technician",
        "time": "2012 - 2016",
    },
    {
        "name": "Industrial High School of Electrical Engineering and Informatics – Ostrava",
        "degree": "High School",
        "field": "Electronic computer systems",
        "time": "2008 - 2012",
    },
]

LANGUAGES = [
    {"name": "Czech", "description": "Native"},
    {"name": "English", "description": "Knowledge for limited work (B1-B2)"},
]
INTERESTS = ["Sports", "Nonsmoker"]
